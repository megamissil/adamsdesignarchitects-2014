<?php
  include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
  $aPage = pageGet( 68 );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Deadlines and Budgets';
	$cSEOTitle = '';
	$layout = 'subpage';
	
  include ('header2.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         <article class="columns medium-9 medium-push-3 ">
           <div class="content">
              <div class="large-9 columns">
                <h1><?php echo $aPage['title']; ?></h1>
                    <?php echo $aPage['msg']; ?>

                    <hr class="divider">
                    <a href="Deadlines.pdf" target="_blank"><img class="charts" src="img/deadlines.png" alt="deadlines"></a>

                    <hr class="divider">
                    <a href="Cost_Chart.pdf" target="_blank"><img class="charts" src="img/cost-chart.png" alt="deadlines"></a>
              </div>
             
           </div>


         </article>
         <aside class="columns medium-3 medium-pull-9 ">
            <?php include ('who-we-are-sidebar.php'); ?>
          </aside>
       </section>
<?php
 include ('footer.php');
?>