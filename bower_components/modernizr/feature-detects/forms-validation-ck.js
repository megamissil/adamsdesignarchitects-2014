// This implementation only tests support for interactive form validation.
// To check validation for a specific type or a specific other constraint,
// the test can be combined: 
//    - Modernizr.inputtypes.numer && Modernizr.formvalidation (browser supports rangeOverflow, typeMismatch etc. for type=number)
//    - Modernizr.input.required && Modernizr.formvalidation (browser supports valueMissing)
//
(function(e,t){t.formvalidationapi=!1;t.formvalidationmessage=!1;t.addTest("formvalidation",function(){var e=createElement("form");if("checkValidity"in e&&"addEventListener"in e){if("reportValidity"in e)return!0;var n=!1,r;t.formvalidationapi=!0;e.addEventListener("submit",function(e){window.opera||e.preventDefault();e.stopPropagation()},!1);e.innerHTML='<input name="modTest" required><button></button>';testStyles("#modernizr form{position:absolute;top:-99999em}",function(i){i.appendChild(e);r=e.getElementsByTagName("input")[0];r.addEventListener("invalid",function(e){n=!0;e.preventDefault();e.stopPropagation()},!1);t.formvalidationmessage=!!r.validationMessage;e.getElementsByTagName("button")[0].click()});return n}return!1})})(document,window.Modernizr);