// binaryType is truthy if there is support.. returns "blob" in new-ish chrome.
// plus.google.com/115535723976198353696/posts/ERN6zYozENV
// github.com/Modernizr/Modernizr/issues/370
Modernizr.addTest("websocketsbinary",function(){var e="https:"==location.protocol?"wss":"ws",t;if("WebSocket"in window){if(t="binaryType"in WebSocket.prototype)return t;try{return!!(new WebSocket(e+"://.")).binaryType}catch(n){}}return!1});