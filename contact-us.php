<?php

  include('postman/_variables.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/tyfoon/connect.php');

	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Contact Us';
	$cSEOTitle = '';
	$layout = 'subpage';
?>

<?php
  include("header.php");
?>
       <article class="row body">
         <div class="columns small-12">
           <hr class="divider" />
           <div class="content">


             <h1>Contact Us</h1>
           </div>


         </div>
         <aside class="columns medium-4 medium-push-8">
           <div class="content">


              <p>Adams Design Associates, Inc.<br>
              Architecture/Interiors/Planning<br>
              205.328.1100 ext. 108<br>
              2 - 20th Street North, Suite 940<br>
              Birmingham, AL 35203<br>
              <a href="mailto:info@adamsdesignarchitects.com">info@adamsdesignarchitects.com</a><br />
              (<a href="https://www.google.com/maps/place/2+20th+St+N+%23940/@33.512893,-86.8085814,17z/data=!3m1!4b1!4m2!3m1!1s0x88891b93a4bb5999:0x2cb1546833897fbd" target="_blank" style="color:#888888;">Google Maps</a>)</p>
            </div>


          </aside>
         <div class="columns medium-8 medium-pull-4">
           <div class="content">
             <form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">

               		<label for="name">Name:</label>
               		<input type="text" name="name" id="name" value="<?=$_POST['name'] ?>"/><br />

               		<label for="email">Email:</label>
               		<input type="text" name="email" id="email" value="<?=$_POST['email'] ?>"/><br />


               		<label for="phone">Phone:</label>
               		<input type="text" name="phone" id="phone" value="<?=$_POST['phone'] ?>"/><br />

               		<label for="msg">Message:</label>
               		<textarea name="msg"><?=$_POST['msg'] ?></textarea><p />
	
               		<button type="submit" name="sbmtbtn" id="sbmtbtn" class="tiny">Send Form</button>

             </form>
           </div>


         </div>
         
       </article>
<?php
 include ('footer.php');
?>
