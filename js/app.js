// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

function init() {
	
	// start up after .5sec no matter what
    window.setTimeout(function(){
        start();
    }, 500);
}

// fade in experience
function start() {
	
	$('body').removeClass("loading").addClass('loaded');
	
}

$(window).load(function() {
	
	// fade in page
	init();

});



$(".photo-cover" ).each(function( index ) {
  var src = $(this).children('img').attr('src');
  $(this).css({ "background-image": "url(" + src + ") " });  
});

$(".photo-cover img").hide();


function resizingStuff() {
	var headerHeight = $('header').outerHeight( true );
	var footerHeight = $('footer').outerHeight( true );
	var contentHeight = $('.primary').outerHeight( true );
	var rotatorHeight = $('.photo-cover').outerHeight( true );
	var pageHeight = $(window).height();
	//var pageWidth = $(window).width();

	var newRotatorHeight = pageHeight - headerHeight - footerHeight - contentHeight;
	
	if ($(window).width() >= 1025) {
		//if (rotatorHeight > newRotatorHeight) {
			$('.photo-cover').css('padding-top', newRotatorHeight);
			$('.rotator, .orbit-container').css('max-height', newRotatorHeight);
		//} else {
		//	$('.photo-cover').css('padding-top', '');
		//	$('.rotator, .orbit-container').css('max-height', '');
		//}
			
	} else {
		$('.photo-cover').css('padding-top', '');
		$('.rotator, .orbit-container').css('max-height', '');
	}
	
	
	
}

// Fire it when the page loads
$(document).ready(function() {

	$('.multiple-items').slick({
	  infinite: true,
	  slidesToShow: 5,
	  slidesToScroll: 5
	});

	$('.carousel').slick({
	  fade : true,
	  autoplay: true
	});
	
	resizingStuff();
	$('body').addClass('loaded');

});


// Fire it on window resize
window.onresize = function() {
    resizingStuff();
};