<?php
  include( $_SERVER['DOCUMENT_ROOT'].'/tyfoon/connect.php' );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = '';
	$cSEOTitle = '';
	$layout = 'home';
  $aFeatured = pageByCategory('NEWS', 'ANY', 0 , 1 , 'PUBL_DESC');

  include ('header.php');
?>
      
      <div class="carousel show-for-large-up">
        <div>
           <div><img src="/img/slide1-1.jpg" width="100%"></div>
           <div class="orbit-caption">
            Architecture/Interiors/Planning
           </div>
        </div>
        <div>
           <div><img src="/img/slide2-1.jpg" width="100%"></div>
           <div class="orbit-caption">
             Helping Clients Reach Goals
            </div>
        </div>
        <div>
           <div><img src="/img/slide3-1.jpg" width="100%"></div>
           <div class="orbit-caption">
             Creating Memorable Places
            </div>
        </div>
        <div>
           <div><img src="/img/slide4-1.jpg" width="100%"></div>
           <div class="orbit-caption">
             Magnifying Strengths
            </div>
        </div>
        <div>
           <div><img src="/img/slide5-1.jpg" width="100%"></div>
        </div>
      </div>

      <div class="carousel show-for-medium-only">
        <div>
           <div><img src="/img/slide1-md.jpg" width="100%"></div>
           <div class="orbit-caption">
            Architecture/Interiors/Planning
           </div>
        </div>
        <div>
           <div><img src="/img/slide2-md.jpg" width="100%"></div>
           <div class="orbit-caption">
             Helping Clients Reach Goals
            </div>
        </div>
        <div>
           <div><img src="/img/slide3-md.jpg" width="100%"></div>
           <div class="orbit-caption">
             Creating Memorable Places
            </div>
        </div>
        <div>
           <div><img src="/img/slide4-md.jpg" width="100%"></div>
           <div class="orbit-caption">
             Magnifying Strengths
            </div>
        </div>
        <div>
           <div><img src="/img/slide5-md.jpg" width="100%"></div>
        </div>
      </div>

      <div class="carousel show-for-small-only">
        <div>
           <div><img src="/img/slide1-sm.jpg" width="100%"></div>
           <div class="orbit-caption">
            Architecture/Interiors/Planning
           </div>
        </div>
        <div>
           <div><img src="/img/slide2-sm.jpg" width="100%"></div>
           <div class="orbit-caption">
             Helping Clients Reach Goals
            </div>
        </div>
        <div>
           <div><img src="/img/slide3-sm.jpg" width="100%"></div>
           <div class="orbit-caption">
             Creating Memorable Places
            </div>
        </div>
        <div>
           <div><img src="/img/slide4-sm.jpg" width="100%"></div>
           <div class="orbit-caption">
             Magnifying Strengths
            </div>
        </div>
        <div>
           <div><img src="/img/slide5-sm.jpg" width="100%"></div>
        </div>
      </div>

       <section class="row primary" data-equalizer>
         <div class="columns medium-3 news" data-equalizer-watch>
            <h1>News</h1>
            <ul class="no-bullet">
              <?php foreach( $aFeatured as $aArticle) {?>
                <li><?php echo '<small>'.date('m/d/Y', strtotime( $aArticle['published'] )).$aArticle['msg_short'].'</small>'; ?>
                <a href="<?php echo $aArticle['url']; ?>"><?php echo $aArticle['title']; ?></a>
                    <a class="read-more" href="<?php echo $aArticle['url']; ?>">View Article</a>
                </li>
              <?php } ?>
            </ul>
            <a href="news.php">More News...</a>
          </div>
         <article class="columns medium-7 welcome" data-equalizer-watch>
           <hr class="hide-for-medium-up" />
           <h1>About Us</h1>
           <p>Adams Design Associates is an architecture, interior design, master planning and consulting firm whose focus is helping clients achieve their goals. We began in 1960 as Cobb/Adams/Benton.  Our designs create a memorable sense of place and magnify the qualities of each client, building site or existing structure. <a href="working-with-us.php">What's it like to work with us?<br />Click here to learn more</a>.</p>
           <a href="client-response.php" class="button tiny">What is important to you ?</a>
         </article>         
       </section>
<?php
 include ('footer.php');
?>
