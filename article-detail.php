<?php
  $aFeatured = pageGet($_GET['cn']);
  $aSidebarNews = pageByCategory('NEWS');
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = $aFeatured['title'];
	$cSEOTitle = '';
	$layout = 'ARTICLE';
  $aSidebarNews = pageByCategory('NEWS', 'ANY', 0 , 25 , 'PUBL_DESC');

	
  include ('header.php');
?>

<section class="row body">
  <div class="columns small-12">
      <hr class="divider" />
  </div>
  
  <article class="columns medium-9 large-9 medium-push-3 large-push-3">
    <div class="content">
<!--       <h2 class="date"></h2> -->
        <div class="large-6 columns">
          <h3><?= $aFeatured['title'] ?></h3>
              <?= $aFeatured['msg'] ?>
              <a href="news.php" class="button">Return to News</a>
        </div>

        <figure class="pad-right-large large-6 columns">
          <ul class="rotator-fade-fix" data-orbit data-options="animation:fade; bullets: true; variable_height: false; slide_number: false; navigation_arrows: false; timer_speed: 3500; next_on_click: true; pause_on_hover: true; resume_on_mouseout: true;">
            <?php if($aFeatured['images']){
                foreach($aFeatured['images'] as $cKey=> $aDocument) {
            ?>                      

            <li><img src="/tyfoon/site/pages/images/<?=$aDocument['photo_path']?>" target="_blank"></li>
                    
            <? } } ?>          
          </ul>
        </figure>
   
    </div>
    </article>

      <aside class="columns medium-3 large-3 medium-pull-9 large-pull-9">
        <?php include_once('article-search.php'); ?>
           <div class="news">
            <ul class="no-bullet">
             <?php foreach( $aSidebarNews as $aArticle) {?>
                <li>
                <?php echo ''.date('m/d/Y', strtotime( $aArticle['published'] )).$aArticle['msg_short']. ''; ?>
                <a href="<?php echo $aArticle['url']; ?>"><?php echo $aArticle['title']; ?></a><br>
                  <a href="<?php echo $aArticle['url']; ?>">Read More ></a>
                 </li>
              <?php } ?>
            </ul>
          </div>
      </aside>
</section>

<?php
 include ('footer.php');
?>