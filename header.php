<?php

require_once ('zeekee-functions.php');

?><!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="robots" content="nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="<?php echo $cMetaDesc; ?>" name="description" />
    <meta content="<?php echo $cMetaKW; ?>" name="keywords" />
    <title><?php if (!empty($cSEOTitle)) { echo $cSEOTitle; } else if (!empty($cPageTitle)) { ?>Adams Design Associates - Architects – <?php echo $cPageTitle; ?><?php } else { ?>Adams Design Associates - Architects - Birmingham, AL<?php } ?></title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    
    <link rel="stylesheet" href="stylesheets/foundation-icons.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/slick.css"/>

    <script src="bower_components/modernizr/modernizr.js"></script>
    <script type="text/javascript" src="//use.typekit.net/mzx7grq.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  </head>
  <body class="<?=$layout ?>">
    <header class="row top">
      <div class="logo columns small-5 medium-2 small-text-center">
        <a href="/index2.php"><img src="/img/logo-new.png" alt="Adams Design Associates Architects"></a>
      </div>
      <div class="columns small-7 medium-10 navigation">
        <nav class="top-bar" data-topbar>
          <ul class="title-area">
            <li class="name">
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"></a></li>
          </ul>

          <section class="top-bar-section">
            <ul class="right">
              <?php
                //make_current_page_active ('Who We Are', '/general-overview.php', 'has-dropdown', TRUE);
              ?>
              <li class="show-for-medium-down"><a href="/index2.php">Home</a></li>
              <li class="has-dropdown"><a href="/quick-facts.php">Who We Are</a>
                <ul class="dropdown">
                  <li><a href="/quick-facts.php">Quick Facts</a></li>
                  <li><a href="/deadlines.php">Deadlines and Budgets</a></li>
                  <li><a href="/working-with-us.php">Working With Us</a></li>
                  <li><a href="/awards.php">Awards</a></li>
                  <li><a href="/testimonials.php">Testimonials</a></li>
                  <?php
                    // Using an active class causes a weird z-index issue on /awards page
                    //make_current_page_active ('General Overview', '/general-overview.php');
                    //make_current_page_active ('Culture', '/culture.php');
                    //make_current_page_active ('Awards &amp; Testimonials', '/awards-and-testimonials.php');
                  ?>
                </ul>
              </li>
              <?php
                make_current_page_active ('Portfolio', '/portfolio.php');
                make_current_page_active ('Facility Consulting Services', '/facility-consulting-services.php');
                make_current_page_active ('News', '/news.php');
                make_current_page_active ('Contact Us', '/contact-us.php');
              ?>
            </ul>
          </section>
        </nav>
      </div>
    </header>
    <main>
