<?php
  include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
  $aPage = pageGet( 69 );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Facility Consulting Services';
	$cSEOTitle = '';
	$layout = 'subpage';
	
  include ('header.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         
         <article class="columns small-12">
           <div class="content">
              <div class="large-6 columns">
                <h1><?php echo $aPage['title']; ?></h1>
                    <?php echo $aPage['msg']; ?>
              </div>
             
             <figure class="pad-right-large large-6">
                 <ul class="rotator-fade-fix" data-orbit data-options="animation:fade; bullets: true; variable_height: false; slide_number: false; navigation_arrows: false; timer_speed: 3500;  next_on_click: true; pause_on_hover: true; resume_on_mouseout: true;">
                   <li>
                     <img src="img/sub5.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub6.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub7.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub8.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub9.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub10.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub11.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                  </ul>
               </figure>
           </div>


         </article>
       </section>
<?php
 include ('footer.php');
?>
