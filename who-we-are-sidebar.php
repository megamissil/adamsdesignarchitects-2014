<?php

require_once ('zeekee-functions.php');

?>
<ul class="side-nav">
  <?php
    make_current_page_active ('Quick Facts', '/quick-facts.php');
    make_current_page_active ('Deadlines & Budgets', '/deadlines.php');
    make_current_page_active ('Working With Us', '/working-with-us.php');
    make_current_page_active ('Awards', '/awards.php');
    make_current_page_active ('Testimonials', '/testimonials.php');
  ?>
</ul>