<?php
  include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
  $aPage = pageGet( 64 );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Quick Facts';
	$cSEOTitle = '';
	$layout = 'subpage';
	
  include ('header2.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         <article class="columns medium-9 medium-push-3 ">
           <div class="content">
              <div class="large-5 columns">
                <h1><?php echo $aPage['title']; ?></h1>
                    <?php echo $aPage['msg']; ?>
              </div>

              <!-- <table class="facts-table">
           <tbody>
        <tr>
            <td>FIRM NAME</td>
            <td>ADAMS DESIGN ASSOCIATES, INC.<br />
            2 &ndash; 20th Street North, Suite 940<br />
            Birmingham, Alabama 35203<br />
            Phone (205) 328-1100 Fax (205) 328-2201</td>
        </tr>
        <tr>
            <td>ESTABLISHED</td>
            <td>1960 as COBB/ADAMS/BENTON</td>
        </tr>
        <tr>
            <td>SERVICES</td>
            <td>Architecture<br />
            Interior Design<br />
            Master Planning<br />
            Consulting</td>
        </tr>
        <tr>
            <td>PHILOSOPHY</td>
            <td>The focus of our practice is helping clients achieve their goals.</td>
        </tr>
        <tr>
            <td>AWARDS</td>
            <td>Received four awards for architectural design since 2004 (two national, one state and one local)</td>
        </tr>
        <tr>
            <td>CONTACT</td>
            <td>L. Paul Roderick,  AIA, NCARB<br />
            (205) 328-1100 x108<br />
            proderick@adamsdesignarchitects.com</td>
        </tr>
    </tbody>
</table> -->

             <figure class="pad-right-large large-6">
                 <ul class="rotator-fade-fix" data-orbit data-options="animation:fade; bullets: true; variable_height: false; slide_number: false; navigation_arrows: false; timer_speed: 3500; next_on_click: true; pause_on_hover: true; resume_on_mouseout: true;">
                  <li>
                     <img src="img/sub5.jpg" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub6.jpg" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub7.jpg" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub8.jpg" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub9.jpg" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub10.jpg" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/sub11.jpg" alt="Museum Large">
                   </li>
                  </ul>
               </figure>
           </div>


         </article>
         <aside class="columns medium-3 medium-pull-9 ">
            <?php include ('who-we-are-sidebar.php'); ?>
          </aside>
       </section>
<?php
 include ('footer.php');
?>
