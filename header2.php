<?php

require_once ('zeekee-functions.php');

?><!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="robots" content="nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="<?php echo $cMetaDesc; ?>" name="description" />
    <meta content="<?php echo $cMetaKW; ?>" name="keywords" />
    <title><?php if (!empty($cSEOTitle)) { echo $cSEOTitle; } else if (!empty($cPageTitle)) { ?>Adams Design Associates - Architects – <?php echo $cPageTitle; ?><?php } else { ?>Adams Design Associates - Architects - Birmingham, AL<?php } ?></title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <?php if ($layout != 'home') {
      echo '<link rel="stylesheet" href="stylesheets/subpage.css" />';
    } ?>
    <link rel="stylesheet" href="stylesheets/foundation-icons.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/slick.css"/>
    
    
    <script src="bower_components/modernizr/modernizr.js"></script>
    <script type="text/javascript" src="//use.typekit.net/mzx7grq.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  </head>
  <body class="<?=$layout ?>">
    <header class="row">
      <div class="logo columns small-5 medium-2 small-text-center">
        <a href="/index2.php"><img src="/img/logo-new.png" width="185" height="127" alt="Adams Design Associates Architects"></a>
      </div>
      <div class="columns small-7 medium-10 navigation">
        <nav class="top-bar" data-topbar>
          <ul class="title-area">
            <li class="name">
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"></a></li>
          </ul>

          <section class="top-bar-section">
            <ul class="right">
              <?php
                 make_current_page_active ('Home', '/index2.php');
                 make_current_page_active ('Who We Are', '/quick-facts.php', 'has-dropdown', FALSE);
                make_current_page_active ('Portfolio', '/portfolio.php');
                make_current_page_active ('Facility Consulting Services', '/facility-consulting-services.php');
                make_current_page_active ('News', '/news.php');
                make_current_page_active ('Contact Us', '/contact-us.php');
              ?>
            </ul>
          </section>
        </nav>
      </div>
    </header>
    <main>