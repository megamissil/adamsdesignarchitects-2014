<?php
  include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
  $aPage = pageGet( $_GET['cn'] );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Portfolio Detail';
	$cSEOTitle = '';
	$layout = 'subpage';
	
  include ('header.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         <article class="columns medium-9 large-10 medium-push-3 large-push-2">
           
           <figure class="columns medium-7 small-12">
             <ul class="rotator-fade-fix" data-orbit data-options="animation:fade; bullets: true; variable_height: false; slide_number: false; navigation_arrows: false; timer_speed: 3000; next_on_click: true; pause_on_hover: true; resume_on_mouseout: true;">
                <?php if($aPage['images']){
                  unset($aPage['images'][1]);
                  foreach($aPage['images'] as $cKey=> $aDocument) {
                ?>                      

                  <li><img src="/tyfoon/site/pages/images/<?=$aDocument['photo_path']?>" target="_blank"></li>
                
          <? } } ?>          
              </ul>
           </figure>

           <div class="columns medium-5 small-12 content">
             <h1><?php echo $aPage['title']; ?></h1>
                    <?php echo $aPage['msg']; ?>
             <a href="/portfolio-cat.php?cat=<?php echo $_GET['cat']?>" class="small-link">&larr; Previous Projects</a>
           </div>
         </article>

         <aside class="columns medium-3 large-2 medium-pull-9 large-pull-10">
            <?php include('portfolio-sidebar.php'); ?>
          </aside>
       </section>
<?php
 include ('footer.php');
?>
