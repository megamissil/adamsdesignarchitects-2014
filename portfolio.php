<?php
   include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
   $aPage = pageByCategory( 'PORTFOLIO' , 'ANY' , 0 , 999 , 'PUBL_ASC');
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Portfolio';
	$cSEOTitle = '';
	$layout = 'subpage';
	
  include ('header.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         <article class="columns medium-9 large-10 medium-push-3 large-push-2">
           <ul class="small-blocks small-block-grid-2 medium-block-grid-3 large-block-grid-4">
             <?php 
             // $x = 1;
             // $max = 12; 
             // while ($max >= $x) {
                foreach( $aPage as $cKey => $aValue ) {
                $aPageID = pageGet ($aValue[pageid]);
                $aPageImage = $aPageID['images'][1];

             ?>
             <li>
               <a href="/portfolio-detail.php?cn=<? echo $aValue['pageid'] ?>" class="portfolio-item">
                 <figure>
                   <div class="image">
                     <img src="/tyfoon/site/pages/images/<?=$aPageImage['photo_path']?>" alt="Museum">
                   </div>
                   <figcaption class="hide-for-medium-down">
                     <div class="capwrapper">
                      <h2><?php echo $aValue['title']; ?></h2>
                       <h3><?php echo $aValue['msg_short']; ?></h3>
                     </div>
                   </figcaption>
                 </figure>
               </a>
             </li>
             <?php 
              // $x ++;
                 }
              ?>
           </ul>
         </article>
         <aside class="columns medium-3 large-2 medium-pull-9 large-pull-10">
           
           <?php include('portfolio-sidebar.php'); ?> 

         </aside>
       </section>
<?php
 include ('footer.php');
?>