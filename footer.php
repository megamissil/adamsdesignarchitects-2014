    </main>
    <footer class="row">
      <div class="columns small-12 text-right small-text-left">
        <hr>
        P&nbsp;&nbsp;205.328.1100 &nbsp;&nbsp;&nbsp;&nbsp; <br class="show-for-small-only" />2 - 20th Street North, Suite 940 &nbsp;&nbsp; <br class="show-for-small-only" />Birmingham, AL 35203 (<a href="https://www.google.com/maps/place/2+20th+St+N+%23940/@33.512893,-86.8085814,17z/data=!3m1!4b1!4m2!3m1!1s0x88891b93a4bb5999:0x2cb1546833897fbd" target="_blank">Google Maps</a>)
        <a style="float:right; padding-left: 0.3em;" href="https://www.facebook.com/adamsdesignarchitects" target="_blank"><img src="/img/fb-logo.png" alt="facebook"></a>
        <a style="float:right;" href="https://www.linkedin.com/company/adams-design-associates/" target="_blank" ><img src="/img/in-logo.png" alt="linked-in"></a>
      </div>

    </footer>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script type="text/javascript" src="js/slick.min.js"></script>
    <script src="js/slippry.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
