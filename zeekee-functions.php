<?php 
function make_current_page_active($pageName, $url, $additionalClasses = '', $dropdown = FALSE) {
  echo '<li';
  if ($url == $_SERVER['REQUEST_URI'] || !empty($additionalClasses)) {
    echo ' class="';
    if ($url == $_SERVER['REQUEST_URI']) {
      echo 'active';
    }
    if (!empty($additionalClasses)) {
      echo ' '.$additionalClasses;
    }
    echo '"';
  }
  echo '><a href="'.$url.'">'.$pageName.'</a>';
  
  if ($dropdown != TRUE)
    echo '</li>';
  
  
}

?>