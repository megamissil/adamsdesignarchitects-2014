<?php
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Culture';
	$cSEOTitle = '';
	$layout = 'subpage';
	
  include ('header2.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         <article class="columns medium-9 medium-push-3 ">
           <div class="content">


              <h1><?php echo $aPage['title']; ?></h1>
                 <?php echo $aPage['msg']; ?>
             <figure class="pad-right-large large-6">
                 <ul class="rotator-fade-fix" data-orbit data-options="animation:fade; bullets: true; variable_height: false; slide_number: false; navigation_arrows: false; timer_speed: 3500; next_on_click: true; pause_on_hover: true; resume_on_mouseout: true;">
                   <li>
                      <img src="img/museum-large.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/museum-large2.jpg" width="1600" height="1067" alt="Museum Large">

                   </li>
                   <li>
                     <img src="img/museum-large3.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                  </ul>
               </figure>
           </div>


         </article>
         <aside class="columns medium-3 medium-pull-9 ">
           <?php include ('who-we-are-sidebar.php'); ?>

          </aside>
       </section>
<?php
 include ('footer.php');
?>
