<?php

require_once ('zeekee-functions.php');

?>
<ul class="side-nav">
  <?php
    make_current_page_active ('Commercial', '/portfolio-cat.php?cat=Commercial');

    make_current_page_active ('Interiors', '/portfolio-cat.php?cat=Interiors');

    make_current_page_active ('Renovations', '/portfolio-cat.php?cat=Renovations');

    make_current_page_active ('Repositioning', '/portfolio-cat.php?cat=Repositioning');

    make_current_page_active ('Multifamily', '/portfolio-cat.php?cat=Multifamily');

    make_current_page_active ('Collegiate', '/portfolio-cat.php?cat=Collegiate');

    make_current_page_active ('Senior Housing', '/portfolio-cat.php?cat=Senior_Housing');

    make_current_page_active ('Athletics', '/portfolio-cat.php?cat=Athletics');

    make_current_page_active ('Schools', '/portfolio-cat.php?cat=Schools');

    make_current_page_active ('Urban Repair', '/portfolio-cat.php?cat=Urban_Repair');

    make_current_page_active ('View All', '/portfolio.php');
  ?>
</ul>