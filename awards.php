<?php
  include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
  $aPage = pageGet( 66 );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Awards';
	$cSEOTitle = '';
	$layout = 'subpage';
	
  include ('header2.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         <article class="columns medium-9 medium-push-3 ">
           <div class="content">

              <div class="large-9 columns">
                <h1><?php echo $aPage['title']; ?></h1>
                    <?php echo $aPage['msg']; ?>
              </div>
<!--              <figure class="pad-right-large large-3">
                 <ul class="rotator-fade-fix" data-orbit data-options="animation:fade; bullets: true; variable_height: false; slide_number: false; navigation_arrows: true; timer_speed: 3500; next_on_click: true; pause_on_hover: false;">
                   <li>
                      <img src="img/slide1.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/slide2.jpg" width="1600" height="1067" alt="Museum Large">

                   </li>
                   <li>
                     <img src="img/slide3.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/slide4.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                  <li>
                     <img src="img/slide5.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                  </ul>
               </figure> -->
           </div>


         </article>
         <aside class="columns medium-3 medium-pull-9 ">
            <?php include ('who-we-are-sidebar.php'); ?>
          </aside>
       </section>
<?php
 include ('footer.php');
?>
