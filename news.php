<?php
  include( $_SERVER['DOCUMENT_ROOT'].'/tyfoon/connect.php' );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'News';
	$cSEOTitle = '';
	$layout = 'subpage';
  $aFeatured = pageByCategory('NEWS', 'ANY', 0 , 25 , 'PUBL_DESC');
  	
  include ('header.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         <div class="columns medium-8 medium-push-4">
           <article class="row">
            <div class="columns medium-8 content">
             <ul class="no-bullet news-ul">
               <?php foreach( $aFeatured as $aArticle) {?>
                <li>
                <?php echo ''.date('m/d/Y', strtotime( $aArticle['published'] )).$aArticle['msg_short'].''; ?><br />
                <a href="<?php echo $aArticle['url']; ?>"><?php echo $aArticle['title']; ?></a><br>

                  <a href="<?php echo $aArticle['url']; ?>">Read More ></a>
                 </li>
                 <hr class="divider" />
              <?php } ?>
              </ul>
             </div>
           </article>
          
         </div>
         <aside class="columns medium-4 medium-pull-8">
           <?php include_once('article-search.php'); ?>
          </aside>
       </section>
<?php
 include ('footer.php');
?>
