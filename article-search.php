<form method="get" class="search-form">
   <div class="panel">
     <div class="row collapse">
       <div class="columns small-10 medium-9">
         <input name="cn" type="search" value="" placeholder="Search" class="search_field">
       </div>
       <div class="columns small-2 medium-3">
         <button type="submit" class="tiny">
           <i class="fi-magnifying-glass"></i>
         </button>
       </div>
     </div>  
   </div>
 </form>