<?php
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Awards &amp; Testimonials';
	$cSEOTitle = '';
	$layout = 'subpage';
	
  include ('header2.php');
?>
       <section class="row body">
         <div class="columns small-12">
           <hr class="divider" />
         </div>
         <article class="columns medium-9 medium-push-3 ">
           <div class="content">


             <h1>Awards &amp; Testimonials</h1>
             <figure class="pad-right-large large-6">
                 <ul class="rotator-fade-fix" data-orbit data-options="animation:fade; bullets: true; variable_height: false; slide_number: false; navigation_arrows: false; timer_speed: 3500; next_on_click: true; pause_on_hover: true; resume_on_mouseout: true;">
                   <li>
                      <img src="img/museum-large.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                   <li>
                     <img src="img/museum-large2.jpg" width="1600" height="1067" alt="Museum Large">

                   </li>
                   <li>
                     <img src="img/museum-large3.jpg" width="1600" height="1067" alt="Museum Large">
                   </li>
                  </ul>
               </figure>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultricies ultrices sem, vel aliquet arcu volutpat vel. Nam eget ligula quis ante placerat condimentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam posuere porttitor tempus. Aenean non congue nisl. Nulla faucibus convallis massa, ac convallis libero interdum id. Duis imperdiet quis neque id rutrum. Morbi tristique mauris eu ultricies malesuada. Nam quis mauris mollis dui iaculis mattis. Fusce lorem nisi, tincidunt quis ligula eget, accumsan semper risus. Nullam vel elit porttitor, blandit erat in, molestie arcu. Pellentesque dapibus, diam eu vehicula lobortis, magna lacus egestas quam, id hendrerit nulla justo at diam.</p>
           </div>


         </article>
         <aside class="columns medium-3 medium-pull-9 ">
            <?php include ('who-we-are-sidebar.php'); ?>
          </aside>
       </section>
<?php
 include ('footer.php');
?>
